package Phone;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.CardLayout;
import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class PhoneGUI {

	private JFrame frame;
	PhoneDial dialog;		// diglog뜨게
	static PhoneGUI window;	// 연락처 추가 나오면 기본 창 꺼지게
	static DefaultListModel model;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new PhoneGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PhoneGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 726, 757);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JPanel panel = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panel, 27, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panel, 33, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel, 127, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panel, 670, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(panel);
		
		JPanel panel_1 = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panel_1, 26, SpringLayout.SOUTH, panel);
		springLayout.putConstraint(SpringLayout.WEST, panel_1, 33, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel_1, 388, SpringLayout.SOUTH, panel);
		SpringLayout sl_panel = new SpringLayout();
		panel.setLayout(sl_panel);
		
		JLabel lbl_list = new JLabel("\uC804\uD654\uBAA9\uB85D");
		lbl_list.setFont(new Font("나눔바른고딕", Font.BOLD, 24));
		sl_panel.putConstraint(SpringLayout.NORTH, lbl_list, 40, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, lbl_list, 287, SpringLayout.WEST, panel);
		panel.add(lbl_list);
		springLayout.putConstraint(SpringLayout.EAST, panel_1, 670, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(panel_1);
		
		JPanel panel_2 = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panel_2, 35, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, panel_2, 33, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel_2, 154, SpringLayout.SOUTH, panel_1);
		panel_1.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3);
		panel_3.setLayout(new CardLayout(0, 0));
		
		model = new DefaultListModel<String>();
		PhoneDAO dao = new PhoneDAO();
		ArrayList<PhoneDTO> alist = new ArrayList<>();
		alist = dao.phoneSerach();
		for(int i = 0 ; i<alist.size();i++) {
			model.addElement(alist.get(i).getName());
		}
		JList list_name = new JList(model);
		panel_3.add(list_name, "name_70908677751668");
		
		JPanel panel_4 = new JPanel();
		panel_1.add(panel_4);
		SpringLayout sl_panel_4 = new SpringLayout();
		panel_4.setLayout(sl_panel_4);
		
		JLabel lbl_name = new JLabel("\uC774\uB984           -");
		sl_panel_4.putConstraint(SpringLayout.NORTH, lbl_name, 58, SpringLayout.NORTH, panel_4);
		sl_panel_4.putConstraint(SpringLayout.WEST, lbl_name, 10, SpringLayout.WEST, panel_4);
		sl_panel_4.putConstraint(SpringLayout.EAST, lbl_name, 95, SpringLayout.WEST, panel_4);
		panel_4.add(lbl_name);
		
		JLabel lbl_age = new JLabel("\uB098\uC774           -");
		sl_panel_4.putConstraint(SpringLayout.NORTH, lbl_age, 81, SpringLayout.SOUTH, lbl_name);
		sl_panel_4.putConstraint(SpringLayout.WEST, lbl_age, 0, SpringLayout.WEST, lbl_name);
		panel_4.add(lbl_age);
		
		JLabel lbl_tel = new JLabel("\uC804\uD654\uBC88\uD638      -");
		sl_panel_4.putConstraint(SpringLayout.WEST, lbl_tel, 0, SpringLayout.WEST, lbl_name);
		sl_panel_4.putConstraint(SpringLayout.SOUTH, lbl_tel, -82, SpringLayout.SOUTH, panel_4);
		panel_4.add(lbl_tel);
		springLayout.putConstraint(SpringLayout.EAST, panel_2, 670, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(panel_2);
		SpringLayout sl_panel_2 = new SpringLayout();
		panel_2.setLayout(sl_panel_2);
		
		JButton btn_TelAdd = new JButton("\uC5F0\uB77D\uCC98\uCD94\uAC00");
		btn_TelAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				window.frame.setVisible(false);// 윈도우의 프레임을 꺼버림
				
				dialog = new PhoneDial();	
				dialog.setVisible(true);	// 연락처 추가 눌렀을때 다이얼로그뜨게
			}
		});
		sl_panel_2.putConstraint(SpringLayout.NORTH, btn_TelAdd, 41, SpringLayout.NORTH, panel_2);
		sl_panel_2.putConstraint(SpringLayout.WEST, btn_TelAdd, 88, SpringLayout.WEST, panel_2);
		panel_2.add(btn_TelAdd);
		
		JButton btn_TelDel = new JButton("\uC5F0\uB77D\uCC98\uC0AD\uC81C");
		sl_panel_2.putConstraint(SpringLayout.NORTH, btn_TelDel, 0, SpringLayout.NORTH, btn_TelAdd);
		sl_panel_2.putConstraint(SpringLayout.EAST, btn_TelDel, -115, SpringLayout.EAST, panel_2);
		panel_2.add(btn_TelDel);
	}
}
