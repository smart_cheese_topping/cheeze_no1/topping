package Phone;

import java.lang.ref.PhantomReference;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PhoneDAO {
	Connection conn = null;
	PreparedStatement psmt = null;
	ArrayList<PhoneDTO> list = new ArrayList<PhoneDTO>();

	public PhoneDAO() {

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");

			String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
			String dbid = "hr";
			String dbpw = "hr";

			DriverManager.getConnection(url, dbid, dbpw);

			conn = DriverManager.getConnection(url, dbid, dbpw);
			if (conn != null) {
				System.out.println("연결성공");
			} else {
				System.out.println("연결실패");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// public void phoneAdd(phoneDTO dto) {
	// String sql = "insert into phone values (?, ?, ?)";
	//
	//
	// try {
	// psmt = conn.prepareStatement(sql);
	//
	// psmt.setString(1, name);
	// psmt.setInt(2, age);
	// psmt.setString(3, tel);
	// } catch (SQLException e) {
	// e.printStackTrace();
	// }
	// }

	public ArrayList<PhoneDTO> phoneSerach() {

		try {
			String sql = "select name from phone";

			psmt = conn.prepareStatement(sql);
			ResultSet rs = psmt.executeQuery();

			while (rs.next()) {

				list.add(new PhoneDTO(rs.getString(1)));

			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
